/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Target preparer to install an app as privileged app by pushing it to /system/priv-app.
 *
 * <p>Requires a device where 'adb root' is possible, typically a userdebug build.</p>
 */
public class AfwTestPrivAppInstaller extends AfwTestTargetPreparer implements ITargetCleaner {

    /**
     * System folder for privileged apps.
     */
    private static final String PRIV_APP_DIR = "/system/priv-app";

    @Option(name = "app-filename",
            description = "app to install")
    private Collection<String> mApps = new ArrayList<String>();

    @Option(name = "cleanup",
            description = "true to delete the apk in teardown")
    private boolean mCleanup = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, DeviceNotAvailableException {

        if (mApps.isEmpty()) {
            return;
        }

        // Enable adb root and remount system partition
        enableAdbRoot(device);
        device.remountSystemWritable();

        for (String app : mApps) {
            String privApp = String.format("%s/%s", PRIV_APP_DIR, app);
            if (device.pushFile(getApk(buildInfo, app), privApp)) {
                CLog.i(String.format("Privileged app installed: %s", privApp));
            } else {
                throw new TargetSetupError(String.format("Failed to install %s", privApp));
            }
        }

        // Reboot to activate the installed apps
        device.reboot();
        device.waitForDeviceAvailable();
        // Enable adb root again
        enableAdbRoot(device);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {
        if (!mCleanup || mApps.isEmpty()) {
            return;
        }

        if (!device.enableAdbRoot()) {
            throw new RuntimeException(
                    String.format("Failed to enable adb root: %s", device.getSerialNumber()));
        }

        device.remountSystemWritable();

        for (String app : mApps) {
            device.executeShellCommand(String.format("rm %s/%s", PRIV_APP_DIR, app));
        }
    }
}

