/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.FileUtil;

import java.io.File;

/**
 * A target cleaner to copy file from the external storage of the device to host after the
 * test execution.
 */
@OptionClass(alias = "afw-test-pull-external-file")
public class AfwTestPullExternalFile extends AfwTestTargetPreparer implements ITargetCleaner {

    @Option(name = "remote-file",
            description = "File to copy from external storage.",
            mandatory = true)
    private String mRemoteFile = null;

    @Option(name = "local-file",
            description = "File path relative to android-cts/repository.",
            mandatory = true)
    private String mLocalFile =  null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo) throws TargetSetupError,
            DeviceNotAvailableException {
        // Do nothing.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {

        try {
            File localFile = new File(getRepositoryDir(buildInfo), mLocalFile);
            if (localFile.getParentFile().mkdirs()) {
                localFile.createNewFile();
            }
            FileUtil.copyFile(device.pullFileFromExternal(mRemoteFile), localFile);
        } catch (DeviceNotAvailableException e1) {
            throw e1;
        } catch (Exception e2) {
            // Log error but not failing the target preparer.
            CLog.e(String.format("Failed to copy remote file.", e2));
        }
    }
}

