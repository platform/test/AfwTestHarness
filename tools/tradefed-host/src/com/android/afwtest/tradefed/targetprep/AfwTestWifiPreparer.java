/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.RunUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A {@link ITargetPreparer} that connects to wifi network with configurations read from file.
 */
@OptionClass(alias = "afw-test-wifi")
public final class AfwTestWifiPreparer extends AfwTestTargetPreparer implements ITargetCleaner {

    @Option(name = "wifi-config-file",
            description = "The file name containing wifi configuration.")
    private String mConfigFileName = null;

    @Option(name = "wifi-ssid-key",
            description = "The key of wifi ssid in the config file.")
    private String mWifiSsidKey = null;

    @Option(name = "wifi-password-key",
            description = "The key of wifi password in the config file.")
    private String mWifiPasswordKey = null;

    @Option(name = "wifi-attempts",
            description = "Number of attempts to connect to wifi network.")
    private int mWifiAttempts = 5;

    @Option(name = "disconnect-wifi-after-test",
            description = "Disconnect from wifi network after test completes.")
    private boolean mDisconnectWifiAfterTest = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo) throws TargetSetupError,
            DeviceNotAvailableException {

        if (mConfigFileName == null) {
            throw new TargetSetupError("wifi-config-file not specified");
        }

        if (mWifiSsidKey == null) {
            throw new TargetSetupError("wifi-ssid-key not specified");
        }

        File configFile = new File(getCtsBuildHelper(buildInfo).getTestCasesDir(), mConfigFileName);
        Properties props = null;
        try {
            props = readProperties(configFile);
        } catch (IOException e) {
            throw new TargetSetupError("Failed to read prop file: " + configFile.getAbsolutePath());
        }

        String wifiSsid = props.getProperty(mWifiSsidKey, "");
        String wifiPassword = props.getProperty(mWifiPasswordKey, "");

        if (wifiSsid.isEmpty()) {
            throw new TargetSetupError(
                    mWifiSsidKey + " not specified in file " + configFile.getAbsolutePath());
        }

        // Implement retry.
        for (int i = 0; i < mWifiAttempts; ++i) {
            try {

                if (device.connectToWifiNetworkIfNeeded(wifiSsid, wifiPassword)) {
                    return;
                }
            } catch(Exception e) {
                CLog.e("Error trying to connect to Wifi:", e);
            }
            boolean lastAttempt = (i + 1) == mWifiAttempts;
            if (!lastAttempt) {
                RunUtil.getDefault().sleep(device.getOptions().getWifiRetryWaitTime());
            }
        }

        throw new TargetSetupError(String.format("Failed to connect to wifi network %s on %s",
                wifiSsid, device.getSerialNumber()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {

        if (mDisconnectWifiAfterTest && device.isWifiEnabled()) {
            if (device.disconnectFromWifi()) {
                CLog.i("Successfully disconnected from wifi network on %s",
                        device.getSerialNumber());
            } else {
                CLog.w("Failed to disconnect from wifi network on %s", device.getSerialNumber());
            }
        }
    }

    /**
     * Help function to read {@link Properties} from a file.
     *
     * @param file {@link File} to read properties from
     * @return {@link Properties} read from given file
     *
     * @throws IOException if read failed
     */
    private static Properties readProperties(File file) throws IOException {
        InputStream input = null;

        try {
            input = new FileInputStream(file);
            Properties props = new Properties();
            props.load(input);
            return props;
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }
}

