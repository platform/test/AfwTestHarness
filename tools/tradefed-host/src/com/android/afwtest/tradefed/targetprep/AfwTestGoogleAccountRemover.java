/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.RunUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * Target preparer to remove Google accounts.
 *
 * <p>Requires a device where 'adb root' is possible, typically a userdebug build.</p>
 *
 * <p>Requires com.anroid.afwtest.systemutil to be pre-installed as privileged app.</p>
 */
@OptionClass(alias = "afw-test-google-account-remover")
public class AfwTestGoogleAccountRemover extends AfwTestTargetPreparer implements ITargetCleaner {

    /**
     * Package name of the system util app.
     */
    private static final String SYSTEM_UTIL_PKG_NAME = "com.android.afwtest.systemutil";

    /**
     * Action to remove Google account.
     */
    private static final String REMOVE_GOOGLE_ACCOUNT_CLASS = ".RemoveGoogleAccount";

    /**
     * Waiting time for system util app.
     */
    private static final long SYSTEM_UTIL_PKG_WAIT_TIME_MS = TimeUnit.MINUTES.toMillis(3);

    @Option(name = "google-account",
            description = "Google accounts to remove, remove all Google accounts if not none given")
    private Collection<String> mGoogleAccounts = new ArrayList<String>();

    @Option(name = "remove-before-test",
            description = "Remove given Google account or all Google accounts before test.")
    private boolean mRemoveBeforeTest = true;

    @Option(name = "remove-after-test",
            description = "Remove given Google account or all Google accounts after the test.")
    private boolean mRemoveAfterTest = true;

    @Option(name = "attempts",
            description = "Number of attempts")
    private int mAttempts = 3;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, DeviceNotAvailableException {
        if (waitForAppPkgInfo(device, SYSTEM_UTIL_PKG_NAME, SYSTEM_UTIL_PKG_WAIT_TIME_MS) == null) {
            throw new TargetSetupError(String.format("%s not installed successfully.",
                    SYSTEM_UTIL_PKG_NAME));
        }

        if (mRemoveBeforeTest) {
            doRemoval(device);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {
        if (waitForAppPkgInfo(device, SYSTEM_UTIL_PKG_NAME, SYSTEM_UTIL_PKG_WAIT_TIME_MS) == null) {
            throw new RuntimeException(String.format("%s not installed successfully.",
                    SYSTEM_UTIL_PKG_NAME));
        }

        if (mRemoveAfterTest) {
            try {
                doRemoval(device);
            } catch (TargetSetupError ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    /**
     * Help function to remove requested Google accounts.
     *
     * @param device test device
     */
    private void doRemoval(ITestDevice device)
            throws TargetSetupError, DeviceNotAvailableException {
        if (mGoogleAccounts.isEmpty()) {
            removeAllAccounts(device);
        } else {
            for (String account : mGoogleAccounts) {
                removeAccount(device, account);
            }
        }
    }

    /**
     * Remove all Google accounts.
     *
     * @param device test device
     */
    private void removeAllAccounts(ITestDevice device)
            throws TargetSetupError, DeviceNotAvailableException {
        String removeCmd = String.format("am instrument -w %s/%s",
                SYSTEM_UTIL_PKG_NAME,
                REMOVE_GOOGLE_ACCOUNT_CLASS);

        if (executeRemoveAccountCommand(device, removeCmd)) {
            CLog.i(String.format("All Google accounts were removed from device %s.",
                    device.getSerialNumber()));
        } else {
            throw new TargetSetupError(
                    String.format("Failed to remove all Google accounts from device %s.",
                            device.getSerialNumber()));
        }
    }

    /**
     * Remove a Google account.
     *
     * @param device test device
     * @param account account to remove
     */
    private void removeAccount(ITestDevice device, String account)
            throws TargetSetupError, DeviceNotAvailableException {
        String removeCmd = String.format("am instrument -w -e account \"%s\" %s/%s",
                account, SYSTEM_UTIL_PKG_NAME, REMOVE_GOOGLE_ACCOUNT_CLASS);
        if (executeRemoveAccountCommand(device, removeCmd)) {
            CLog.i(String.format("Google account %s was removed from device %s.",
                    account, device.getSerialNumber()));
        } else {
            throw new TargetSetupError(
                    String.format("Failed to remove Google account %s from device %s.",
                            account, device.getSerialNumber()));
        }
    }

    /**
     * Executes shell command to remove Google account.
     *
     * @param device test device
     * @param cmd command to execute
     * @return {@code true} if success, {@code false} otherwise
     */
    private boolean executeRemoveAccountCommand(ITestDevice device, String cmd)
            throws DeviceNotAvailableException {
        CLog.v(cmd);
        for (int i = 0; i < mAttempts; ++i) {
            CLog.i(String.format("Removing account on device %s, #%d",
                    device.getSerialNumber(), i + 1));

            String result = device.executeShellCommand(cmd);
            // expected result format is (on success):
            //
            // INSTRUMENTATION_RESULT: result=SUCCESS
            // INSTRUMENTATION_CODE: -1
            //
            if (result != null && result.contains("result=SUCCESS")) {
                return true;
            } else {
                CLog.e(String.format("Failed to remove account from device %s: %s.",
                        device.getSerialNumber(), result));
                // Sleep for 15 seconds before next attempt
                RunUtil.getDefault().sleep(TimeUnit.SECONDS.toMillis(15));
            }
        }

        // OK, failed after all attempts
        return false;
    }
}

