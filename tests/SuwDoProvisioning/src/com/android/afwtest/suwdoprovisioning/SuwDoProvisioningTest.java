/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.suwdoprovisioning;

import static com.android.afwtest.uiautomator.Constants.TESTDPC_PKG_NAME;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.support.test.uiautomator.UiAutomatorTestCase;

import com.android.afwtest.common.AccountManagerUtils;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.provisioning.AutomationDriver;
import com.android.afwtest.uiautomator.test.AfwTestUiWatcher;

/**
 * SuW device owner provisioning test.
 */
public class SuwDoProvisioningTest extends UiAutomatorTestCase {

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();

        AfwTestUiWatcher.register(getUiDevice());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown() throws Exception {
        AfwTestUiWatcher.unregister(getUiDevice());

        super.tearDown();
    }

    /**
     * Tests Device Owner provisioning flow.
     */
    public void testDoProvisioning() throws Exception {

        AccountManagerUtils.startAddGoogleAccountActivity(getContext(), true);

        AutomationDriver runner = new AutomationDriver(getUiDevice());
        assertTrue("SuW DO provisioning didn't finish",
                runner.runSuwDoProvisioning(TestConfig.getDefault()));

        DevicePolicyManager devicePolicyManager =
                (DevicePolicyManager) getContext().getSystemService(Context.DEVICE_POLICY_SERVICE);

        // Verify if the device is provisioned.
        assertTrue("Provisioning failed",
                devicePolicyManager.isDeviceOwnerApp(TESTDPC_PKG_NAME));
    }

    /**
     * Gets application context.
     *
     * @return application context
     */
    protected Context getContext() {
        return getInstrumentation().getContext();
    }
}
