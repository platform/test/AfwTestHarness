/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.nfcprovisioning;

import static com.android.afwtest.common.test.TestConfig.DEFAULT_TEST_CONFIG_FILE;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.support.test.uiautomator.UiAutomatorTestCase;
import android.util.Log;

import com.android.afwtest.common.nfcprovisioning.Utils;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.provisioning.AutomationDriver;
import com.android.afwtest.uiautomator.test.AfwTestUiWatcher;

/**
 * NFC provisioning test.
 */
public class NfcProvisioningTest extends UiAutomatorTestCase {

    private static final String TAG = "afwtest.NfcProvisioningTest";

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();

        AfwTestUiWatcher.register(getUiDevice());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown() throws Exception {
        AfwTestUiWatcher.unregister(getUiDevice());

        super.tearDown();
    }

    /**
     * Gets application context.
     *
     * @return application context
     */
    protected Context getContext() {
        return getInstrumentation().getContext();
    }

    public void testNfcProvisioning() throws Exception {

        // Skip the test if no nfc
        if (!getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
            Log.w(TAG, "Device doesn't support NFC, skipping Nfc provisioning test!");
            return;
        }

        // Verify if NFC is available and enabled
        NfcManager nfcManager = (NfcManager) getContext().getSystemService(Context.NFC_SERVICE);
        assertNotNull("Failed to get NfcManager", nfcManager);
        NfcAdapter nfcAdapter = nfcManager.getDefaultAdapter();
        assertNotNull("No NFC adapter found!", nfcAdapter);
        assertTrue("NFC is disabled.", nfcAdapter.isEnabled());

        // Start provisioning
        String deviceAdminPkgName = Utils.startProvisioning(getContext(), DEFAULT_TEST_CONFIG_FILE);
        assertNotNull(deviceAdminPkgName);

        // Navigate the pages.
        AutomationDriver driver = new AutomationDriver(getUiDevice());
        assertTrue("NFC provisioning didn't finish",
                driver.runNfcProvisioning(TestConfig.getDefault()));

        DevicePolicyManager devicePolicyManager =
                (DevicePolicyManager) getContext().getSystemService(Context.DEVICE_POLICY_SERVICE);

        // Verify if the device is provisioned.
        assertTrue("Provisioning failed", devicePolicyManager.isDeviceOwnerApp(deviceAdminPkgName));
    }
}
