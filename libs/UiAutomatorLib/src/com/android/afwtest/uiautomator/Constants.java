/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import java.util.regex.Pattern;

/**
 * Constants for this lib and test packages.
 */
public final class Constants {

    /**
     * Android package name.
     */
    public static final String ANDROID_PKG_NAME = "android";

    /**
     * GMSCore package name.
     */
    public static final String GMS_PKG_NAME = "com.google.android.gms";

    /**
     * Managed provisioning package name.
     */
    public static final String MANAGED_PROVISIONING_PKG_NAME = "com.android.managedprovisioning";

    /**
     * Android package installer package name.
     */
    public static final String PACKAGE_INSTALLER_PKG_NAME = "com.android.packageinstaller";

    /**
     * TestDpc package name.
     */
    public static final String TESTDPC_PKG_NAME = "com.afwsamples.testdpc";

    /**
     * Default file name for provisioning performance stats.
     */
    public static final String PROVISIONING_STATS_FILE = "Provisioning-Stats.csv";

    /**
     * Representation name of TestDpc work profile creation time.
     */
    public static final String STAT_TESTDPC_WORK_PROFILE_CREATION_TIME =
            "testDPC Work Profile creation";

    /**
     * Representation name of Managed Provisioning work profile creation name.
     */
    public static final String STAT_MANAGED_PROVISIONING_WORK_PROFILE_CREATION_TIME =
            "MP Work Profile creation";

    /**
     * Regular expression string to match resource id of GMSCore NEXT button.
     */
    public static final String GMS_NEXT_BTN_RESOURCE_ID_REGEX =
            GMS_PKG_NAME + ":id/(auth_setup_wizard_navbar_next|suw_navbar_next)";

    /**
     * {@link BySelector} for {@link Button} in GMS core with description "ACCEPT".
     */
    public static final BySelector GMS_ACCEPT_BUTTON_SELECTOR = By.pkg(GMS_PKG_NAME).clazz(
            Button.class.getName()).desc("ACCEPT");

    /**
     * {@link BySelector} for {@link Button} in GMS core with description "NEXT".
     */
    public static final BySelector GMS_NEXT_BUTTON_SELECTOR = By.pkg(GMS_PKG_NAME).clazz(
            Button.class.getName()).desc("NEXT");

    /**
     * Resource Id {@link BySelector} for GmsCore NEXT button.
     */
    public static final BySelector GMS_NEXT_BUTTON_RES_SELECTOR =
            By.res(Pattern.compile(GMS_NEXT_BTN_RESOURCE_ID_REGEX))
                    .enabled(true)
                    .clickable(true);

    /**
     * {@link BySelector} for {@link EditText} in GMS core.
     */
    public static final BySelector GMS_TEXT_FIELD_SELECTOR = By.pkg(GMS_PKG_NAME).clazz(
            EditText.class.getName());

    /**
     * {@link BySelector} for {@link CheckBox} in GMS core with resource-id "agree_backup".
     */
    public static final BySelector GMS_CHECK_BOX_SELECTOR = By.res(GMS_PKG_NAME, "agree_backup");

    /**
     * @{@link BySelector} for downloading MDMs in GmsCore.
     */
    public static final BySelector GMS_DOWNLOADING_DIALOG_SELECTOR =
            By.text(Pattern.compile("Downloading.*"))
                    .res(ANDROID_PKG_NAME, "message")
                    .pkg(GMS_PKG_NAME);

    /**
     * @{@link BySelector} unique to the add account page of GmsCore.
     */
    public static final BySelector GMS_ADD_ACCOUNT_PAGE_SELECTOR =
            By.pkg(GMS_PKG_NAME).desc("Add your account");

    /**
     * {@link BySelector} for {@link Button} in ManagedProvisioning with with
     * resource-id suw_navbar_next.
     */
    public static final BySelector MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR = By.res(
            MANAGED_PROVISIONING_PKG_NAME, "suw_navbar_next");

    /**
     * {@link BySelector} in ManagedProvisioning.
     */
    public static final BySelector MANAGED_PROVISIONING_PKG_SELECTOR = By.pkg(
            MANAGED_PROVISIONING_PKG_NAME);


    /**
     * {@link BySelector} for "Learn more" button on some Managed Provisioning dialog.
     */
    public static final BySelector MANAGED_PROVISIONING_LEARN_MORE_LINK_SELECTOR =
            By.res(MANAGED_PROVISIONING_PKG_NAME, "learn_more_link")
                    .enabled(true)
                    .clickable(true);

    /**
     * {@link BySelector} for {@link Button} in ManagedProvisioning with resource-id
     * positive_button.
     */
    public static final BySelector MANAGED_PROVISIONING_OK_BUTTON_SELECTOR = By.res(
            MANAGED_PROVISIONING_PKG_NAME, "positive_button");

    /**
     * {@link BySelector} for {@link Button} in Package Installer with resource-id ok_button.
     */
    public static final BySelector PACKAGE_INSTALLER_INSTALL_BUTTON_SELECTOR = By.res(
            PACKAGE_INSTALLER_PKG_NAME, "ok_button");

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id setup_device_owner on
     * TestDpc Setup Management page.
     */
    public static final BySelector TESTDPC_SETUP_DEVICE_OWNER_RADIO_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "setup_device_owner")
                    .clazz(RadioButton.class.getName())
                    .clickable(true)
                    .checkable(true);

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id setup_managed_profile on
     * TestDpc Setup Management page.
     */
    public static final BySelector TESTDPC_SETUP_MANAGED_PROFILE_RADIO_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "setup_managed_profile")
                    .clazz(RadioButton.class.getName())
                    .clickable(true)
                    .checkable(true);

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id add_account on
     * TestDpc Setup finished page.
     */
    public static final BySelector TESTDPC_ADD_ACCOUNT_RADIO_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "add_account")
                    .clazz(RadioButton.class.getName())
                    .clickable(true)
                    .checkable(true);

    /**
     * {@link BySelector} for the 'NEXT' navigate button on TestDpc pages.
     */
    public static final BySelector TESTDPC_NEXT_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "suw_navbar_next")
                    .clazz(Button.class.getName())
                    .clickable(true);

    /**
     * {@link BySelector} for Managed Provisioning error message.
     */
    public static final BySelector MANAGED_PROVISIONING_ERROR_MSG_SELECTOR =
            By.pkg(MANAGED_PROVISIONING_PKG_NAME).res(ANDROID_PKG_NAME, "message");

    /**
     * {@link BySelector} for Android platform alert message.
     */
    public static final BySelector ANDROID_ALERT_MSG_SELECTOR = By.res(ANDROID_PKG_NAME, "message");

    /**
     * {@link BySelector} for Android platform button.
     */
    public static final BySelector ANDROID_DIALOG_BTN_SELECTOR = By.clazz(Button.class.getName());

    /**
     * {@link BySelector} for {@link Button} in TestDpc with resource-id finish_setup.
     */
    public static final BySelector TESTDPC_FINISH_SKIP_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "btn_add_account_skip");

    /**
     * Private constructor to prevent instantiation.
     */
    private Constants() {
    }
}
