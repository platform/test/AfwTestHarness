/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.utils;

import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.Direction;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Widget utils.
 */
public class WidgetUtils {

    private static final String TAG = "afwtest.WidgetUtils";

    /**
     * Waiting time for each call to UiDevice.wait().
     */
    private static final long DEFAULT_UI_WAIT_TIME_MS = TimeUnit.SECONDS.toMillis(5);

    /**
     * Clicks on given {@link UiObject2} without throwing any exception.
     *
     * @param obj {@link UiObject2} to click
     * @return {@code true} if clicked, {@code false} otherwise
     */
    public static boolean safeClick(UiObject2 obj) {
        String widgetProps = getWidgetPropertiesAsString(obj);
        try {
            obj.click();
            Log.d(TAG, String.format("Clicked: %s", widgetProps));
            return true;
        } catch(Exception e) {
            Log.e(TAG, String.format("Failed to click: %s", widgetProps) , e);
            return false;
        }
    }


    /**
     * Tries to click any of the given list of buttons; return if any button
     * clicked successfully.
     *
     * @param btns list of buttons to click
     * @return {@code true} if any button clicked successfully; {@code false} otherwise
     */
    public static boolean safeClickAny(List<UiObject2> btns) {
        for (UiObject2 obj : btns) {
            if (safeClick(obj)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Perform fling gesture on given {@link UiObject2} until it cannot scroll any more without
     * throwing any exception.
     *
     * @param obj {@link UiObject2} to scroll
     * @param direction The direction in which to fling
     * @return {@code true} if fling performed, {@code false} otherwise
     */
    public static boolean safeFling(UiObject2 obj, Direction direction) {
        String widgetProps = getWidgetPropertiesAsString(obj);

        try {
            // Set limit to 100 times.
            for (int i = 0; i < 100; ++i) {
                if (!obj.fling(direction)) {
                    break;
                }
            }
            Log.d(TAG, String.format("Scrolled: %s", widgetProps));
            return true;
        } catch(Exception e) {
            Log.e(TAG, String.format("Failed to scroll: %s", widgetProps), e);
            return false;
        }
    }

    /**
     * Waits for a widget without throwing any exception (N attempts).
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @param attempts number of attempts.
     * @return {@link UiObject2} if expected widget appears within timeout, {@code null} otherwise
     */
    public static UiObject2 safeWait(UiDevice uiDevice, BySelector selector, long timeoutMS,
            int attempts) {
        for (int i = 0; i < attempts; ++i) {
            UiObject2 widget = safeWait(uiDevice, selector, timeoutMS);
            if (widget != null) {
                return  widget;
            }
        }

        return null;
    }

    /**
     * Waits for a widget without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@link UiObject2} if expected widget appears within timeout, {@code null} otherwise
     */
    public static UiObject2 safeWait(UiDevice uiDevice, BySelector selector, long timeoutMS) {
        try {
            return uiDevice.wait(Until.findObject(selector), timeoutMS);
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait for widget ", e);
        }

        return null;
    }

    /**
     * Waits for a widget without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @return {@link UiObject2} if expected widget appears within default timeout,
     *         {@code null} otherwise
     */
    public static UiObject2 safeWait(UiDevice uiDevice, BySelector selector) {
        return safeWait(uiDevice, selector, DEFAULT_UI_WAIT_TIME_MS);
    }

    /**
     * Waits for a widget and click on it without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @return {@code true} if click action was performed, {@code false} otherwise.
     */
    public static boolean safeWaitAndClick(UiDevice uiDevice, BySelector selector) {
        try {
            return waitAndClick(uiDevice, selector);
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait and click for widget ", e);
        }
        return false;
    }

    /**
     * Waits for a widget and click on it.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @return {@code true} if click action was performed, {@code false} otherwise
     */
    public static boolean waitAndClick(UiDevice uiDevice, BySelector selector)
            throws Exception {
        return waitAndClick(uiDevice, selector, DEFAULT_UI_WAIT_TIME_MS);
    }

    /**
     * Waits for a widget and click on it without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@code true} if click action was performed, {@code false} otherwise.
     */
    public static boolean safeWaitAndClick(UiDevice uiDevice, BySelector selector,
            long timeoutMS) {
        try {
            return waitAndClick(uiDevice, selector, timeoutMS);
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait and click for widget ", e);
        }
        return false;
    }

    /**
     * Waits for a widget and click on it (N attempts).
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @param attempts number of attempts
     * @return {@code true} if click action was performed, {@code false} otherwise
     */
    public static boolean waitAndClick(UiDevice uiDevice, BySelector selector, long timeoutMS,
            int attempts) throws Exception {
        for (int i = 0; i < attempts; ++i) {
            if(waitAndClick(uiDevice, selector, timeoutMS)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Waits for a widget and click on it.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@code true} if click action was performed, {@code false} otherwise
     */
    public static boolean waitAndClick(UiDevice uiDevice, BySelector selector, long timeoutMS)
            throws Exception {
        UiObject2 object = uiDevice.wait(Until.findObject(selector), timeoutMS);
        if (object == null) {
            throw new Exception(String.format("UI object not found: %s", selector.toString()));
        }
        object.click();
        return true;
    }

    /**
     * Waits for all elements with given {@link BySelector} to be gone.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the UI elements to wait
     * @param timeoutMs timeout in mmilliseconds
     */
    public static void waitToBeGone(UiDevice uiDevice, BySelector selector, long timeoutMs)
            throws Exception {
        uiDevice.wait(Until.gone(selector), timeoutMs);
    }

    /**
     * Waits for all elements with given {@link BySelector} to be gone.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the UI elements to wait
     */
    public static void waitToBeGone(UiDevice uiDevice, BySelector selector) throws Exception {
        waitToBeGone(uiDevice, selector, DEFAULT_UI_WAIT_TIME_MS);
    }


    /**
     * Gets properties of a {@link UiObject2} as a String, for debugging purpose.
     *
     * @param obj {@link UiObject2} to get properties from
     * @return properties of given {@link UiObject2} as String
     */
    public static String getWidgetPropertiesAsString(UiObject2 widget) {
        try {
            return String.format("text=[%s],desc=[%s],res=[%s],pkg=[%s],class=[%s]",
                    widget.getText(),
                    widget.getContentDescription(),
                    widget.getResourceName(),
                    widget.getApplicationPackage(),
                    widget.getClassName());
        } catch (Exception e) {
            Log.e(TAG, "Failed to get properties from a widget", e);
        }

        return null;
    }

    /**
     * Gets the package name of a {@link UiObject2} safely.
     *
     * @param widget {@link UiObject2} to get property from
     * @return package name of given widget or null if there is any error
     */
    public static String getPackageName(UiObject2 widget) {
        try {
            return widget.getApplicationPackage();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get package name from a widget", e);
        }

        return null;
    }

    /**
     * Gets the text of a {@link UiObject2} safely.
     *
     * @param widget {@link UiObject2} to get property from
     * @return text of given widget or null if there is any error
     */
    public static String getText(UiObject2 widget) {
        try {
            return widget.getText();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get text from a widget", e);
        }

        return null;
    }

    /**
     * Gets the content description of a {@link UiObject2} safely.
     *
     * @param widget {@link UiObject2} to get property from
     * @return content description of given widget or null if there is any error
     */
    public static String getContentDescription(UiObject2 widget) {
        try {
            return widget.getContentDescription();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get text from a widget", e);
        }

        return null;
    }
}
