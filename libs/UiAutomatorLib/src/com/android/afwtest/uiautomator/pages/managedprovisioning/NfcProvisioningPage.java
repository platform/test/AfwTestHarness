/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.managedprovisioning;

import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_SELECTOR;

import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;

/**
 * A {@link UiPage} to manage NFC provisioning process.
 */
public final class NfcProvisioningPage extends BasePage {

    private static final String TAG = "afwtest.NfcProvisioningPage";

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public NfcProvisioningPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return MANAGED_PROVISIONING_PKG_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        if (!waitForProvisioningToFinish()) {
            throw new RuntimeException("NFC Provisioning timeout");
        }
    }
}
