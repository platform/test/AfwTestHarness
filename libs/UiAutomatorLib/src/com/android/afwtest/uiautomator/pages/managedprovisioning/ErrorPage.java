/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.managedprovisioning;

import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_NAME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * A {@link UiPage} representing error dialog popped up from Managed Provisioning.
 */
public class ErrorPage extends BasePage {

    /**
     * Default UI waiting time, in milliseconds.
     */
    private static final long DEFAULT_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(10);

    private static final BySelector ERROR_PAGE_SELECTOR =
            By.pkg(MANAGED_PROVISIONING_PKG_NAME).text("Oops!");

    private static final BySelector OK_BUTTON_SELECTOR =
            By.pkg(MANAGED_PROVISIONING_PKG_NAME)
                   .text(Pattern.compile("[Oo][Kk]"))
                   .clickable(true);

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public ErrorPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return ERROR_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        WidgetUtils.waitAndClick(getUiDevice(), OK_BUTTON_SELECTOR, DEFAULT_TIMEOUT_MS);
    }
}
