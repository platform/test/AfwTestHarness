/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages;

import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;

/**
 * A {@link UiPage} that won't navigate any further.
 *
 * <p>Such page is usually used as the last page of an automation flow.</p>
 */
public final class LandingPage extends UiPage {

    /**
     * Unique element that this page should wait for.
     */
    private final BySelector mUniqueElementSelector;

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     * @param uniqueElementSelector {@link BySelector} of the ui element to wait for
     */
    public LandingPage(UiDevice uiDevice,
            TestConfig config,
            BySelector uniqueElementSelector) {
        super(uiDevice, config);
        mUniqueElementSelector =  uniqueElementSelector;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return mUniqueElementSelector;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        // Do nothing.
    }
}
