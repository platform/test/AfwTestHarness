/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.testdpc;

import static com.android.afwtest.uiautomator.Constants.STAT_TESTDPC_WORK_PROFILE_CREATION_TIME;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_NEXT_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_PKG_NAME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

/**
 * Setup finished page, which should be launched after DO or PO provisioning is successful.
 */
public final class SetupFinishPage extends UiPage {

    /**
     * Flag indicating if account was migrated.
     */
    private final boolean mIsAccountMigrated;

    /**
     * {@link BySelector} for TestDpc SuW layout title "Finish setup".
     */
    private static final BySelector TESTDPC_FINISH_SETUP_PAGE_TITLE_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "suw_layout_title")
                    .text("Finish setup");

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     * @param isAccountMigrated whether account was migrated
     */
    public SetupFinishPage(UiDevice uiDevice, TestConfig config, boolean isAccountMigrated) {
        super(uiDevice, config);
        mIsAccountMigrated = isAccountMigrated;
    }

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public SetupFinishPage(UiDevice uiDevice, TestConfig config) {
        this(uiDevice, config, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return TESTDPC_FINISH_SETUP_PAGE_TITLE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        if (mIsAccountMigrated) {
            // Find message that account migration was successful.
            BySelector succeedResult =
                    By.pkg(TESTDPC_PKG_NAME).text("Added account that is now managed:");

            // Find managed work account name to verify required account is migrated.
            BySelector managedAccount =
                    By.res(TESTDPC_PKG_NAME, "managed_account_name")
                            .text(getTestConfig().getWorkAccountUsername().toLowerCase());

            if (WidgetUtils.safeWait(getUiDevice(), succeedResult) == null ||
                    WidgetUtils.safeWait(getUiDevice(), managedAccount) == null) {
                throw new RuntimeException(String.format(
                        "Provisioning failed: %s is not setup to be managed by TestDpc!",
                         getTestConfig().getWorkAccountUsername()));
            }

            // Save Time metric
            getProvisioningStatsLogger().stopTime(STAT_TESTDPC_WORK_PROFILE_CREATION_TIME);
            getProvisioningStatsLogger().writeStatsToFile();

        } else {
            BySelector succeedResult = By.pkg(TESTDPC_PKG_NAME)
                    .text("To manage the new managed profile, visit the badged version of this");
            WidgetUtils.safeWait(getUiDevice(), succeedResult);
        }

        WidgetUtils.waitAndClick(getUiDevice(), TESTDPC_NEXT_BUTTON_SELECTOR);
    }
}
