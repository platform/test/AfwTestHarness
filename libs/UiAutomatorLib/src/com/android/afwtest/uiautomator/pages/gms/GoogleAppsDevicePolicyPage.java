/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.gms;

import static com.android.afwtest.uiautomator.Constants.GMS_DOWNLOADING_DIALOG_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_NEXT_BUTTON_RES_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_PKG_NAME;

import android.os.SystemClock;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.Timer;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.concurrent.TimeUnit;

/**
 * GMS Google Apps device policy page.
 */
public final class GoogleAppsDevicePolicyPage extends UiPage {

    /**
     * Timeout for starting downloading mdm.
     */
    private static final long START_DOWNLOAD_MDM_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(5);

    /**
     * Timeout for downloading the dmd.
     */
    private static final long DOWNLOAD_MDM_TIMEOUT_MS = TimeUnit.MINUTES.toMillis(1);

    /**
     * Number of attempts to start downloading MDM.
     *
     * <p>
     * It's found on some devices it takes some time for the MDM app icon to load;
     * and before the icon is loaded, clicking the Next button doesn't work.
     * Add retry logic to keep clicking until the downloading starts.
     * </p>
     */
    private static final int CLICK_NEXT_BTN_ATTEMPTS = 5;

    /**
     * {@link BySelector} for the MDM app icon.
     */
    private static final BySelector MDM_ICON_SELECTOR =
            By.res(GMS_PKG_NAME, "auth_device_management_download_app_icon");

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public GoogleAppsDevicePolicyPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return MDM_ICON_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        // Clicks the Next button until downloading starts.
        int attempts = CLICK_NEXT_BTN_ATTEMPTS;
        while (getUiDevice().hasObject(GMS_NEXT_BUTTON_RES_SELECTOR) && attempts > 0) {

            --attempts;

            // Clicks the next button to start downloading mdm
            WidgetUtils.safeClick(getUiDevice().findObject(GMS_NEXT_BUTTON_RES_SELECTOR));

            // Wait for downloading mdm to appear
            if (WidgetUtils.safeWait(getUiDevice(),
                    GMS_DOWNLOADING_DIALOG_SELECTOR, START_DOWNLOAD_MDM_TIMEOUT_MS) != null) {
                break;
            }
        }

        // Waits until the download finishes
        Timer timer = new Timer(DOWNLOAD_MDM_TIMEOUT_MS);
        timer.start();
        while (!timer.isTimeUp() && getUiDevice().hasObject(GMS_DOWNLOADING_DIALOG_SELECTOR)) {
            SystemClock.sleep(TimeUnit.SECONDS.toMillis(2));
        }
    }
}
