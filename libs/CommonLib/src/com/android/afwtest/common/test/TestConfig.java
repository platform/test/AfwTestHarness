/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common.test;

import static com.android.afwtest.common.Constants.KEY_APP_CRASH_WHITELIST;
import static com.android.afwtest.common.Constants.KEY_DEVICE_ADMIN_PKG_CHECKSUM;
import static com.android.afwtest.common.Constants.KEY_DEVICE_ADMIN_PKG_LOCATION;
import static com.android.afwtest.common.Constants.KEY_DEVICE_ADMIN_PKG_NAME;
import static com.android.afwtest.common.Constants.KEY_DEVICE_ADMIN_PKG_SIGNATURE_HASH;
import static com.android.afwtest.common.Constants.KEY_MUTE_APP_CRASH_DIALOGS;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGETS;
import static com.android.afwtest.common.Constants.KEY_TEST_TIMEOUT_MIN;
import static com.android.afwtest.common.Constants.KEY_TIMEOUT_SIZE;
import static com.android.afwtest.common.Constants.KEY_WIFI_PWD;
import static com.android.afwtest.common.Constants.KEY_WIFI_SECURITY_TYPE;
import static com.android.afwtest.common.Constants.KEY_WIFI_SSID;
import static com.android.afwtest.common.Constants.KEY_WORK_ACCOUNT_PASSWORD;
import static com.android.afwtest.common.Constants.KEY_WORK_ACCOUNT_USERNAME;

import android.util.Log;

import com.android.afwtest.common.FileUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * A singleton class that reads properties from afw-test.props and provides interfaces
 * to get test configurations.
 */
public final class TestConfig {

    private static final String TAG = "afwtest.TestConfig";

    /**
     * Mapping from timeout size to its integer value.
     */
    private static final Map<String, Integer> TIMEOUT_SIZE_MAPPING =
            new HashMap<String, Integer>() {{
                put("S", 2);
                put("M", 3);
                put("L", 5);
                put("XL", 8);
                put("XXL", 13);
            }};

    /**
     * File path of the default test config file.
     */
    public static final String DEFAULT_TEST_CONFIG_FILE = "/data/local/tmp/afw-test.props";

    // Mapping from test configuration file path and its loaded {@link TestConfig} object.
    private static Map<String, TestConfig> sTestConfigs = new HashMap<String, TestConfig>();

    // Store configurations loaded from a file.
    private final Properties mProps;

    /**
     * Creates a new object by loading configurations from file.
     *
     * @param configFilePath path of the test configuration file
     */
    private TestConfig(String configFilePath) throws IOException {
        mProps = FileUtils.readPropertiesFromFile(configFilePath);
    }

    /**
     * Gets {@link TestConfig} object for given file path.
     *
     * @param configFilePath file path of the test configuration
     * @return {@link TestConfig} object for given file path
     */
    public static TestConfig get(String configFilePath) throws IOException {
        if (sTestConfigs.containsKey(configFilePath)) {
            return sTestConfigs.get(configFilePath);
        }

        // Create new object.
        TestConfig testConfig = new TestConfig(configFilePath);
        sTestConfigs.put(configFilePath, testConfig);
        return testConfig;
    }

    /**
     * Gets {@link TestConfig} object for {@link #DEFAULT_TEST_CONFIG_FILE}.
     *
     * @return {@link TestConfig} object for {@link #DEFAULT_TEST_CONFIG_FILE}
     */
    public static TestConfig getDefault() throws IOException {
        return get(DEFAULT_TEST_CONFIG_FILE);
    }

    /**
     * Gets device admin package name.
     *
     * @return device admin package name
     */
    public String getDeviceAdminPkgName() {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_NAME);
    }

    /**
     * Gets device admin package name.
     *
     * @param defaultValue default value if device admin pkg name is not found
     * @return device admin package name
     */
    public String getDeviceAdminPkgName(String defaultValue) {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_NAME, defaultValue);
    }

    /**
     * Gets device admin package location.
     *
     * @return device admin package location
     */
    public String getDeviceAdminPkgLocation() {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_LOCATION);
    }

    /**
     * Gets device admin package location.
     *
     * @param defaultValue default value if device admin package location is not found
     * @return device admin package location
     */
    public String getDeviceAdminPkgLocation(String defaultValue) {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_LOCATION, defaultValue);
    }

    /**
     * Gets device admin package checksum.
     *
     * @return device admin package checksum
     */
    public String getDeviceAdminPkgChecksum() {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_CHECKSUM);
    }

    /**
     * Gets device admin package checksum.
     *
     * @param defaultValue default valud if device admin package checksum is not found
     * @return device admin package checksum
     */
    public String getDeviceAdminPkgChecksum(String defaultValue) {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_CHECKSUM, defaultValue);
    }

    /**
     * Gets device admin package signature hash.
     *
     * @return device admin package signature hash
     */
    public String getDeviceAdminPkgSignatureHash() {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_SIGNATURE_HASH);
    }

    /**
     * Gets device admin package signature hash.
     *
     * @param defaultValue default value if device admin package signature hash is not found
     * @return device admin package signature hash
     */
    public String getDeviceAdminPkgSignatureHash(String defaultValue) {
        return mProps.getProperty(KEY_DEVICE_ADMIN_PKG_SIGNATURE_HASH, defaultValue);
    }

    /**
     * Gets timeout size value from props file.
     *
     * <p>
     * Possible size values are strings "S", "M", "L", "XL" or "XXL".
     * </p>
     *
     * @return An integer corresponding to the timeout size configured in the config file.
     */
    public int getTimeoutSize() {
        // Default to M
        String timeoutSize = getProperty(KEY_TIMEOUT_SIZE, "M");
        if (!TIMEOUT_SIZE_MAPPING.containsKey(timeoutSize)) {
            Log.w(TAG, "Invalid timeout size, defaulting to M");
            timeoutSize = "M";
        }

        return TIMEOUT_SIZE_MAPPING.get(timeoutSize);
    }

    /**
     * Gets test timeout in minutes.
     *
     * @return test timeout in minutes or -1 if either timeout is not specified or invalid
     */
    public int getTestTimeoutMin() {
        return getTestTimeoutMin(-1);
    }

    /**
     * Gets test timeout in minutes.
     *
     * @param defaultValue default value if test timeout not specified
     * @return test timeout in minutes
     */
    public int getTestTimeoutMin(int defaultValue) {
        String value = mProps.getProperty(KEY_TEST_TIMEOUT_MIN);
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        return Integer.valueOf(value);
    }

    /**
     * Gets Wifi SSID.
     *
     * @return Wifi SSID
     */
    public String getWifiSsid() {
        return mProps.getProperty(KEY_WIFI_SSID);
    }

    /**
     * Gets Wifi SSID.
     *
     * @param defaultValue default value if Wifi SSID is not found
     * @return Wifi SSID
     */
    public String getWifiSsid(String defaultValue) {
        return mProps.getProperty(KEY_WIFI_SSID, defaultValue);
    }

    /**
     * Gets Wifi password.
     *
     * @return Wifi password
     */
    public String getWifiPassword() {
        return mProps.getProperty(KEY_WIFI_PWD);
    }

    /**
     * Gets Wifi password.
     *
     * @param defaultValue default value if Wifi password is not found
     * @return Wifi password
     */
    public String getWifiPassword(String defaultValue) {
        return mProps.getProperty(KEY_WIFI_PWD, defaultValue);
    }

    /**
     * Gets Wifi security type.
     *
     * @return Wifi security type
     */
    public String getWifiSecurityType() {
        return mProps.getProperty(KEY_WIFI_SECURITY_TYPE);
    }

    /**
     * Gets Wifi security type.
     *
     * @param defaultValue default value if Wifi security type is not found
     * @return Wifi security type
     */
    public String getWifiSecurityType(String defaultValue) {
        return mProps.getProperty(KEY_WIFI_SECURITY_TYPE, defaultValue);
    }

    /**
     * Gets work account user name.
     *
     * @return work account user name
     */
    public String getWorkAccountUsername() {
        return mProps.getProperty(KEY_WORK_ACCOUNT_USERNAME);
    }

    /**
     * Gets work account user name.
     *
     * @param defaultValue default value if work account user name is not found
     * @return work account user name
     */
    public String getWorkAccountUsername(String defaultValue) {
        return mProps.getProperty(KEY_WORK_ACCOUNT_USERNAME, defaultValue);
    }

    /**
     * Gets work account password.
     *
     * @return work account password
     */
    public String getWorkAccountPassword() {
        return mProps.getProperty(KEY_WORK_ACCOUNT_PASSWORD);
    }

    /**
     * Gets work account password.
     *
     * @param defaultValue default value if work account password is not found
     * @return work account password
     */
    public String getWorkAccountPassword(String defaultValue) {
        return mProps.getProperty(KEY_WORK_ACCOUNT_PASSWORD, defaultValue);
    }

    /**
     * Gets the property of a specific key.
     *
     * @param key property key
     * @return property value of the given key
     *         If the key doesn't exist in the configuration file, null will be returned;
     *         If the value of the key is empty, empty string will be returned
     */
    public String getProperty(String key) {
        return mProps.getProperty(key);
    }

    /**
     * Gets the property of a specific key.
     *
     * @param key property key
     * @param defaultValue default value if given key is not found
     * @return property value of the given key;
     *         if given key not found, {@link #defaultValue} is returned
     */
    public String getProperty(String key, String defaultValue) {
        return mProps.getProperty(key, defaultValue);
    }

    /**
     * Gets the OEM widgets as a list.
     *
     * @return {@link List} of {@link OemWidget}
     */
    public List<OemWidget> getOemWidgets() {
        List<OemWidget> oemWidgets = new LinkedList<OemWidget>();

        String idList = mProps.getProperty(KEY_OEM_WIDGETS, "");
        if (!idList.isEmpty()) {
            String[] ids = idList.split(",");
            for (String id : ids) {
                OemWidget oemWidget = new OemWidget(this, id);
                oemWidgets.add(oemWidget);
            }
        }

        return oemWidgets;
    }

    /**
     * Whether to mute app crash dialogs.
     *
     * @return {@code true} if yes, {@code false} otherwise
     */
    public boolean muteAppCrashDialogs() {
        return Boolean.parseBoolean(mProps.getProperty(KEY_MUTE_APP_CRASH_DIALOGS, "true"));
    }

    /**
     * Gets list of app names that if they crash, just auto close the dialog.
     *
     * @return set of app names that in the app crash dialog message, all in lower case
     */
    public List<String> getAppCrashWhitelist() {
        String[] apps = getProperty(KEY_APP_CRASH_WHITELIST, "").split(",");
        List<String> results = new LinkedList(Arrays.asList(apps));
        results.removeAll(Arrays.asList("", null));
        return results;
    }
}
