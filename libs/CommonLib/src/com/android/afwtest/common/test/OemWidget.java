/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common.test;

import static com.android.afwtest.common.Constants.ACTION_CLICK;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGET_ACTION;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGET_CLASS;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGET_DESCRIPTION;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGET_PACKAGE;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGET_RESOURCE_ID;
import static com.android.afwtest.common.Constants.KEY_OEM_WIDGET_TEXT;

import android.util.Log;

import com.android.afwtest.common.Preconditions;

/**
 * A data class to store properties of an OEM widget.
 */
public final class OemWidget {

    private static final String TAG = "afwtest.OemWidget";

    // Id of the OEM widget.
    private final String mId;

    // Text property of this widget.
    private final String mText;

    // Description property of this widget.
    private final String mDescription;

    // Resource id of this widget.
    private final String mResourceId;

    // Package name of this widget.
    private final String mPackage;

    // Class name of this widget.
    private final String mClass;

    // Action of this widget, any of {@link com.android.afwtest.common.Constants#ACTION_CLICK},
    // {@link com.android.afwtest.common.Constants#ACTION_CHECK} or
    // {@link com.android.afwtest.common.Constants#ACTION_SCROLL}.
    private final String mAction;

    /**
     * Constructs a {@link OemWidget} from configuration file.
     *
     * @param testConfig Path of the test configuration file
     * @param widgetId id of the widget to construct.
     */
    public OemWidget(TestConfig testConfig, String widgetId) {
        // Check Id.
        Preconditions.checkNotEmpty(widgetId);

        Log.d(TAG, "Creating OemWidget " + widgetId);

        mId = widgetId;

        mText = testConfig.getProperty(widgetId + "." + KEY_OEM_WIDGET_TEXT, "");
        mDescription = testConfig.getProperty(widgetId + "." + KEY_OEM_WIDGET_DESCRIPTION, "");
        mResourceId = testConfig.getProperty(widgetId + "." + KEY_OEM_WIDGET_RESOURCE_ID, "");
        mPackage = testConfig.getProperty(widgetId + "." + KEY_OEM_WIDGET_PACKAGE, "");
        mClass = testConfig.getProperty(widgetId + "." + KEY_OEM_WIDGET_CLASS, "");
        mAction = testConfig.getProperty(widgetId + "." + KEY_OEM_WIDGET_ACTION, ACTION_CLICK);

        // At least one property is specified.
        Preconditions.checkNotEmpty(mText + mDescription + mResourceId + mPackage + mClass);

        Log.d(TAG, "OemWidget: " + toString());
    }

    /**
     * Gets id of this widget.
     *
     * @return id of this widget
     */
    public String getId() {
        return mId;
    }

    /**
     * Gets text property of this widget.
     *
     * @return text property of this widget
     */
    public String getText() {
        return mText;
    }

    /**
     * Gets description property of this widget.
     *
     * @return description property of this widget
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Gets resource id of this widget.
     *
     * @return resournce id of this widget
     */
    public String getResourceId() {
        return mResourceId;
    }

    /**
     * Gets package name of this widget.
     *
     * @return package name of this widget
     */
    public String getPackage() {
        return mPackage;
    }

    /**
     * Gets class name of this widget.
     *
     * @return class name of this widget
     */
    public String getClassName() {
        return mClass;
    }

    /**
     * Gets action of this widget.
     *
     * @return action of this widget
     */
    public String getAction() {
        return mAction;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "OemWidget[Id=%s,text=[%s],desc=[%s],res=[%s],pkg=[%s],class=[%s],action=[%s]]",
                mId, mText, mDescription, mResourceId, mPackage, mClass, mAction);
    }
}
