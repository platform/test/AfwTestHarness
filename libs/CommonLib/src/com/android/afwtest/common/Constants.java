/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common;

/***
 * Common constants.
 */
public final class Constants {

    /**
     * Property key of device admin package name, used in the test configuration file.
     */
    public static final String KEY_DEVICE_ADMIN_PKG_NAME = "device_admin_pkg_name";

    /**
     * Property key of device admin package location, used in the test configuration file.
     */
    public static final String KEY_DEVICE_ADMIN_PKG_LOCATION = "device_admin_pkg_location";

    /**
     * Property key of device admin package checksum, used in the test configuration file.
     */
    public static final String KEY_DEVICE_ADMIN_PKG_CHECKSUM = "device_admin_pkg_checksum";

    /**
     * Property key of device admin package signature hash, used in the test configuration file.
     */
    public static final String KEY_DEVICE_ADMIN_PKG_SIGNATURE_HASH
            = "device_admin_pkg_signature_hash";

    /**
     * Property key of timeout size, used in the test configuration file.
     */
    public static final String KEY_TIMEOUT_SIZE = "timeout_size";

    /**
     * Property key of test timeout in minute, used in the test configuration file.
     */
    public static final String KEY_TEST_TIMEOUT_MIN = "test_timeout_min";

    /**
     * Property key of wifi ssid, used in the test configuration file.
     */
    public static final String KEY_WIFI_SSID = "wifi_ssid";

    /**
     * Property key of the wifi password, used in the test configuration file.
     */
    public static final String KEY_WIFI_PWD = "wifi_password";

    /**
     * Property key of the wifi security type, used in the test configuration file.
     */
    public static final String KEY_WIFI_SECURITY_TYPE = "wifi_security_type";

    /**
     * Property key of the username of the work account, used in the test configuration file.
     */
    public static final String KEY_WORK_ACCOUNT_USERNAME = "work_account_username";

    /**
     * Property key of the password of the work account, used in the test configuration file.
     */
    public static final String KEY_WORK_ACCOUNT_PASSWORD = "work_account_password";

    /**
     * Property key for the list of OEM customized widget Ids.
     *
     * <p>The properties of each OEM widget are specified by separate keys in the format of
     * widget_id.property. "property" can be any of {@link #KEY_OEM_WIDGET_TEXT},
     * {@link #KEY_OEM_WIDGET_DESCRIPTION}, {@link #KEY_OEM_WIDGET_RESOURCE_ID},
     * {@link #KEY_OEM_WIDGET_PACKAGE}, {@link #KEY_OEM_WIDGET_CLASS} or
     * {@link #KEY_OEM_WIDGET_ACTION}.</p>
     *
     * <p>For example, OEM can define a widget like:
     * <ul>
     * <li>oem_widgets=custom_widget</li>
     * <li>custom_widget.text=Get started</li>
     * <li>custom_widget.class=android.widget.Button</li>
     * <li>custom_widget.action=click</li>
     * </ul>
     * This tells the test to find a widget with text "Get started" and with class name
     * "android.widget.Button" to click.</p>
     */
    public static final String KEY_OEM_WIDGETS = "oem_widgets";

    /**
     * Property key for the text of an OEM widget.
     */
    public static final String KEY_OEM_WIDGET_TEXT = "text";

    /**
     * Property key for the content description of an OEM widget.
     */
    public static final String KEY_OEM_WIDGET_DESCRIPTION = "description";

    /**
     * Property key for the resource ID of an OEM widget.
     */
    public static final String KEY_OEM_WIDGET_RESOURCE_ID = "resource_id";

    /**
     * Property key for the package name of an OEM widget.
     */
    public static final String KEY_OEM_WIDGET_PACKAGE = "package";

    /**
     * Property key for the class name of an OEM widget.
     */
    public static final String KEY_OEM_WIDGET_CLASS = "class";

    /**
     * Property key for the action on an OEM widget. It can any of {@link #ACTION_CLICK},
     * {@link #ACTION_CHECK} or {@link #ACTION_SCROLL}.
     */
    public static final String KEY_OEM_WIDGET_ACTION = "action";

    /**
     * Property key for the app crash dialog auto close strategy: whether to auto close
     * all non-fatal crashes.
     */
    public static final String KEY_MUTE_APP_CRASH_DIALOGS = "mute_app_crash_dialogs";

    /**
     * Property key for app names whose app crash dialog should be auto closed, separated by ",".
     */
    public static final String KEY_APP_CRASH_WHITELIST = "app_crash_whitelist";

    /**
     * Click action on a widget.
     */
    public static final String ACTION_CLICK = "click";

    /**
     * Scroll action on a widget.
     */
    public static final String ACTION_SCROLL = "scroll";

    /**
     * Check action on a widget, either a checkbox or radio button.
     */
    public static final String ACTION_CHECK = "check";

    /**
     * Constant string for the user to specify the path of the file
     * that contains NFC provisioning configurations.
     */
    public static final String NFC_BUMP_FILE = "NFCBumpFile";

    /**
     * Package name of system util app.
     */
    public static final String SYSTEM_UTIL_PKG_NAME = "com.android.afwtest.systemutil";

    /**
     * Send NFC bump action.
     */
    public static final String ACTION_SEND_NFC_BUMP
            = "com.android.afwtest.systemutil.action.SEND_NFC_BUMP";

    /**
     * Private constructor to prevent instantiation.
     */
    private Constants() {
    }
}
