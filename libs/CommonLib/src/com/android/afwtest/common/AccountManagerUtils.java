/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common;

import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.io.IOException;

/**
 * Help class to wrap AccountManager ralated functionalities.
 */
public final class AccountManagerUtils {

    private static final String TAG = "afwtest.AccountManagerUtils";

    private static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    private static final String EXTRA_SETUP_WIZARD = "setupWizard";

    /**
     * Private constructor to prevent instantiation.
     */
    private AccountManagerUtils() {
    }

    /**
     * Starts Add Account activity by firing a proper intent.
     *
     * @param context {@link Context} object
     * @param isSetupWizard {@code true} if simulating setup wizard,
     *                      {@code false} if simulating Settings->Add Account
     */
    public static void startAddGoogleAccountActivity(Context context, boolean isSetupWizard)
            throws IOException, AuthenticatorException, OperationCanceledException {
        final AccountManager accountManager = AccountManager.get(context);

        // Options for the Add Account activity.
        Bundle options = new Bundle();
        options.putBoolean(EXTRA_SETUP_WIZARD, isSetupWizard);
        if (isSetupWizard) {
            // Skip "Got another device?" page
            options.putBoolean("suppress_device_to_device_setup", true);
        }

        AccountManagerFuture<Bundle> amf = accountManager.addAccount(
                GOOGLE_ACCOUNT_TYPE,
                null, /* authTokenType*/
                null, /* requiredFeatures */
                options,
                null, /* Activity context, null to not start the intent automatically */
                null, /* callback */
                null /* handler */
        );

        // Fire the intent to start the UI.
        Intent intent = (Intent) amf.getResult().get(AccountManager.KEY_INTENT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
