#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

AFW_TH_TESTCASES_OUT := $(HOST_OUT)/afw-th/android-cts/repository/testcases

# default module config filename
AFW_TEST_MODULE_TEST_CONFIG := AndroidTest.xml

# customized test package build rule
BUILD_AFW_TEST_PACKAGE := test/AfwTestHarness/build/test_package.mk
BUILD_AFW_TEST_SUPPORT_PACKAGE := test/AfwTestHarness/build/support_package.mk
BUILD_AFW_TEST_MODULE_TEST_CONFIG := test/AfwTestHarness/build/module_test_config.mk

