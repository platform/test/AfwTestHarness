#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Builds a package and defines a rule to generate the associated test
# package XML needed by the tradefed.
#
# Replace "include $(BUILD_PACKAGE)" with "include $(BUILD_AFW_TEST_PACKAGE)"
#

LOCAL_DEX_PREOPT := false
LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_AFW_TEST_SUPPORT_PACKAGE)
include $(BUILD_AFW_TEST_MODULE_TEST_CONFIG)

afw_test_src_dirs := $(LOCAL_PATH)
afw_test_src_dirs += $(sort $(dir $(LOCAL_GENERATED_SOURCES)))
afw_test_src_dirs := $(addprefix -s , $(afw_test_src_dirs))

afw_test_package_xml := $(AFW_TH_TESTCASES_OUT)/$(LOCAL_PACKAGE_NAME).xml
$(afw_test_package_xml): PRIVATE_SRC_DIRS := $(afw_test_src_dirs)
$(afw_test_package_xml): PRIVATE_INSTRUMENTATION := $(LOCAL_INSTRUMENTATION_FOR)
$(afw_test_package_xml): PRIVATE_PACKAGE := $(LOCAL_PACKAGE_NAME)
$(afw_test_package_xml): PRIVATE_AFW_TEST_PACKAGE_NAME_ := com.android.afwtest.$(notdir $(LOCAL_PATH))
$(afw_test_package_xml): PRIVATE_TEST_PACKAGE := $(PRIVATE_AFW_TEST_PACKAGE_NAME_)
$(afw_test_package_xml): PRIVATE_MANIFEST := $(LOCAL_PATH)/AndroidManifest.xml
$(afw_test_package_xml): PRIVATE_TEST_TYPE := ''
$(afw_test_package_xml): $(afw_test_support_apks)
$(afw_test_package_xml): $(afw_test_module_test_config)
$(afw_test_package_xml): $(CTS_JAVA_TEST_SCANNER_DOCLET) $(CTS_JAVA_TEST_SCANNER) $(CTS_XML_GENERATOR)
	$(hide) echo Generating test description for java package $(PRIVATE_PACKAGE)
	$(hide) mkdir -p $(AFW_TH_TESTCASES_OUT)
	$(hide) $(CTS_JAVA_TEST_SCANNER) \
		$(PRIVATE_SRC_DIRS) \
		-d $(CTS_JAVA_TEST_SCANNER_DOCLET) | \
		$(CTS_XML_GENERATOR) \
		-t $(PRIVATE_TEST_TYPE) \
		-m $(PRIVATE_MANIFEST) \
		-i "$(PRIVATE_INSTRUMENTATION)" \
		-n $(PRIVATE_PACKAGE) \
		-p $(PRIVATE_TEST_PACKAGE) \
		-e $(CTS_EXPECTATIONS) \
		-b $(CTS_UNSUPPORTED_ABIS) \
		-a $(CTS_TARGET_ARCH) \
		-o $@

$(my_register_name) : $(afw_test_package_xml) $(afw_test_module_test_config)
